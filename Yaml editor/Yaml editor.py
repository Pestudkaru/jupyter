
# coding: utf-8

# In[1]:


# Read lines- then lambda and try to use string operators- like .replace ("$i","filename")
# Can get some help from dictionaries file and a11.try.py
# also can do a forloop just for the different ones but then if else to get more variables


# In[175]:


import os
os.chdir('/home/gunnar/Desktop/JuPyteR/Yaml editor/')# r in front of a string
text = open('example_yaml.yaml').read()
samples=open("sample_sheet.txt").readlines()[1:]

for x in range(0,len(samples)):
    temp = text
    temp = temp.replace('$i',samples[x].split()[0])
    temp = temp.replace('$j',samples[x].split()[1])
    temp = temp.replace('$k',samples[x].split()[2])
    file=open("sample_" + str(x+1) + ".yaml","w+")
    file.write(temp)
    file.close()

